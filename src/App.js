import logo from './logo.svg';
import './App.css';
import { TableComponent } from 'table-component-ant'
const users = [
  {
    id: 1,
    name: 'cuonglv',
    phone: '0912121212212',
    address: 'HN',
    active: true,
    job: {
      title: 'Developer',
      company: 'Rnd'
    },
    createdAt: 'Tue Jan 25 2022 10:50:47 GMT+0700'
  },
  {
    id: 1,
    name: 'cuonglv',
    phone: '022032323232',
    address: 'HN',
    active: true,
    job: {
      title: 'Sales',
      company: 'Rnd'
    },
    createdAt: 'Tue Jan 25 2022 11:50:47 GMT+0700'
  },
  {
    id: 2,
    name: 'abc',
    phone: '012104104',
    address: 'HCM',
    active: true,
    job: {
      title: 'PM',
      company: 'Rnd'
    },
    createdAt: 'Tue Jan 25 2022 12:50:47 GMT+0700'
  },
  {
    id: 3,
    name: 'xyz',
    phone: '354353',
    address: 'HN',
    active: false,
    job: {
      title: 'Developer',
      company: 'Rnd'
    },
    createdAt: 'Tue Jan 25 2022 11:50:47 GMT+0700'
  },
  {
    id: 4,
    name: 'Họ tên',
    phone: '1241241241',
    address: 'HN',
    active: false,
    job: {
      title: 'Developer',
      company: 'Rnd'
    },
    createdAt: 'Tue Jan 25 2022 11:50:47 GMT+0700'
  },
]

let COLUMNS = [
  {
    title: 'STT',
    property: '_',
    type: 'stt',
    width: 50,
    fixed: true
  },
  {
    title: 'Họ tên',
    property: 'name',
    fixed: true
  },
  {
    title: 'SĐT',
    property: 'phone'
  },
  {
    title: 'Chức vụ',
    property: 'job',
    type: 'obj',
    attributes: 'title'
  },
  {
    title: 'Công ty',
    property: 'job',
    type: 'obj',
    attributes: 'company'
  },
  {
    title: 'Trạng thái',
    property: 'active',
    type: 'customize',
    customColumn: (col, data, key) => {
      if (data) {
        return <p>Hoạt động</p>
      } else {
        return <p>Ngừng hoạt động</p>
      }
    }
  },
  {
    title: 'Thời gian tạo',
    property: 'createdAt',
    type: 'datetime'
  },
  {
    title: 'Hành động',
    type: 'view-edit-delete',
  },
]

function App() {
  const onTableChange = (page, pageSize) => {
    console.log(page, pageSize)
  }
  const onDelete = (data) => {
    console.log(data)
  }
  const onView = (data) => {
    console.log(data)
  }
  const onEdit = (data) => {
    console.log(data)
  }

  return (
    <div className="App">
      <TableComponent
        data={users}
        columns={COLUMNS}
        pagination={true}
        pageSize={3}
        pageSizeOptions={[3, 5, 10, 20]}
        total={5}
        onTableChange={onTableChange}
        onEdit={onEdit}
        onDelete={onDelete}
        onView={onView}
      />
    </div>
  );
}

export default App;
